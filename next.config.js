/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable import/no-extraneous-dependencies */
const withCSS = require('@zeit/next-css');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = withCSS({
  webpack: (config) => {
    config.resolve.plugins.push(new TsconfigPathsPlugin());
    return config;
  },
});
