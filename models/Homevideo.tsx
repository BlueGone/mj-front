import * as rt from 'runtypes';
import { find, findOne, count } from 'models/strapiHelpers';

export interface Homevideo {
  url: string;
}

const homevideoRuntype: rt.Runtype<Homevideo> = rt.Record({
  url: rt.String,
});

const homevideosPath = 'homevideos';

export const findHomevideos = find(homevideosPath, homevideoRuntype);
export const findOneHomevideo = findOne(homevideosPath, homevideoRuntype);
export const countHomevideos = count(homevideosPath);
