import * as rt from 'runtypes';
import axios from 'axios';
import { URLSearchParams } from 'url';

import { STRAPI_BASE_URL } from 'config.json';

export interface Media {
  url: string;
}

export const mediaRuntype: rt.Runtype<Media> = rt.Record({
  url: rt.String,
});

type FilterSuffix =
    'eq'
  | 'ne'
  | 'lt'
  | 'gt'
  | 'lte'
  | 'gte'
  | 'in'
  | 'nin'
  | 'contains'
  | 'ncontains'
  | 'containss'
  | 'ncontainss'
  | 'null';

export interface Filter<T> {
  suffix: FilterSuffix;
  field: keyof T;
  value: string;
}

export function makeFilter<T>(field: keyof T, suffix: FilterSuffix, value: string): Filter<T> {
  return ({ field, suffix, value });
}

export function resolveMediaUrl(media: Media): string {
  return `${STRAPI_BASE_URL}${media.url}`;
}

export function find<T>(path: string, runtype: rt.Runtype<T>) {
  return (filters: Filter<T>[] = []): Promise<T[]> => axios
    .get<T[]>(`${STRAPI_BASE_URL}/${path}`, {
      params: new URLSearchParams(
        filters.map(({ field, suffix, value }): [string, string] => [`${field}_${suffix}`, value]),
      ),
    })
    .then((response) => rt.Array(runtype).check(response.data));
}

export function findOne<T>(path: string, runtype: rt.Runtype<T>) {
  return (id: string): Promise<T> => axios
    .get<T>(`${STRAPI_BASE_URL}/${path}/${id}`)
    .then((response) => runtype.check(response.data));
}

export function count<T>(path: string) {
  return (): Promise<number> => axios
    .get<number>(`${STRAPI_BASE_URL}/${path}/count`)
    .then((response) => rt.Number.check(response.data));
}
