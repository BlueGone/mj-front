import * as rt from 'runtypes';
import {
  find, findOne, count, Media, mediaRuntype,
} from 'models/strapiHelpers';

export interface Project {
  title: string;
  description: string;
  credits: string;
  readableUrlPath: string;
  images: Media[];
}

const projectRuntype: rt.Runtype<Project> = rt.Record({
  title: rt.String,
  description: rt.String,
  credits: rt.String,
  readableUrlPath: rt.String,
  images: rt.Array(mediaRuntype),
});

const projectsPath = 'projects';

export const findprojects = find(projectsPath, projectRuntype);
export const findOneproject = findOne(projectsPath, projectRuntype);
export const countprojects = count(projectsPath);
