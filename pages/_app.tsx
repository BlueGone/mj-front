import React from 'react';
import App from 'next/app';

import 'antd/dist/antd.css';

export default class MyApp extends App {
  render(): JSX.Element {
    const { Component, pageProps } = this.props;
    return (
      <>
        <style global jsx>
          {`
            @font-face {
              font-family: 'Abril Fatface';
              src: url("/fonts/AbrilFatface-Regular.ttf");
            }

            body {
              background: rgb(216,184,242);
              background: linear-gradient(180deg, rgba(216,184,242,1) 0%, rgba(255,255,255,1) 100%);
            }

            .ant-layout { background: transparent; }

            img.rounded { border-radius: 50%; }

            .ant-layout-header {
              padding: 30px;
              height: auto;
              background: transparent;
            }

            .ant-typography {
              font-family: 'Abril Fatface';
              color: white !important;
              font-size: 1.8em;
            }

            .dark.ant-typography {
              color: #390d58 !important;
            }
            
            h1.ant-typography { font-size: 4.0em; }
            h2.ant-typography { font-size: 3.4em; }

            .fullscreen-menu-root {
              position: absolute;
              width: 100%;
              height: 100%;
              justify-content: center;
              z-index: 1;
              background: rgba(216,184,242, 0.7);
              background: linear-gradient(180deg, rgba(216,184,242,0.7) 0%, rgba(255,255,255,0.7) 100%);
            }

            .visible {
              visibility: visible;
              opacity: 1;
              transition: opacity 0.3s linear;
            }

            .hidden {
              visibility: hidden;
              opacity: 0;
              transition: visibility 0s 0.3s, opacity 0.3s linear;
            }
          `}
        </style>
        <Component {...pageProps} />
      </>
    );
  }
}
