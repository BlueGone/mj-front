import { NextPage } from 'next';

import { Home, HomeProps } from 'components/Home';
import { WithPageBaseLayout } from 'components/PageBaseLayout';

import { findHomevideos } from 'models/Homevideo';

const HomePage: NextPage<HomeProps> = WithPageBaseLayout(Home);

HomePage.getInitialProps = (): Promise<HomeProps> => findHomevideos()
  .then((homevideos) => ({ homevideos }));

export default HomePage;
