import { NextPage } from 'next';

import { Contact } from 'components/Contact';
import { WithPageBaseLayout } from 'components/PageBaseLayout';

const ContactPage: NextPage = WithPageBaseLayout(Contact);

export default ContactPage;
