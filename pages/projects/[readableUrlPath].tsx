import { findprojects, Project } from 'models/Project';
import { WithNextError, makeNextNotFoundError, ResOrNextError } from 'components/WithNextError';
import { makeFilter } from 'models/strapiHelpers';
import ProjectComponent from 'components/Project';
import { WithPageBaseLayout } from 'components/PageBaseLayout';

const page = WithNextError(WithPageBaseLayout(ProjectComponent));

page.getInitialProps = ({ query }): Promise<ResOrNextError<Project>> => findprojects([
  makeFilter('readableUrlPath', 'eq', `${query.readableUrlPath}`),
])
  .then((projects) => (projects[0]
    ? { res: projects[0] }
    : { err: makeNextNotFoundError() }));

export default page;
