import * as React from 'react';
import { Layout } from 'antd';
import { Header } from 'components/Header';

export const PageBaseLayout: React.FC = ({ children }) => (
  <Layout>
    <Header />
    { children }
  </Layout>
);

export function WithPageBaseLayout<TProps>(
  Children: React.ComponentType<TProps>,
): React.FC<TProps> {
  const newComponent: React.FC<TProps> = (props: TProps) => (
    <PageBaseLayout>
      <Children {...props} />
    </PageBaseLayout>
  );
  newComponent.displayName = WithPageBaseLayout.name;
  return newComponent;
}
