import * as React from 'react';
import { NextPage } from 'next';
import ErrorPage, { ErrorProps } from 'next/error';
import { XOR } from 'ts-xor';

export type NextError = ErrorProps;
export type ResOrNextError<TRes> = XOR<
  { res: TRes },
  { err: NextError }
>

export const makeNextError = (statusCode: number, title?: string): NextError => ({
  statusCode, title,
});

export const makeNextNotFoundError = (): NextError => makeNextError(404);

export function WithNextError<TProps>(
  Children: React.ComponentType<TProps>,
): NextPage<XOR<
    { res: TProps },
    { err: NextError }
  >> {
  /* eslint-disable react/destructuring-assignment */
  const component: React.FC<ResOrNextError<TProps>> = (props) => ((props.err)
    ? <ErrorPage {...props.err} />
    : <Children {...props.res} />);
  /* eslint-enable react/destructuring-assignment */
  component.displayName = WithNextError.name;
  return component;
}
