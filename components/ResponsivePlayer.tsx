import * as React from 'react';
import ReactPlayer, { ReactPlayerProps } from 'react-player';
import { ResponsiveIframeWrapper, ResponsiveIframeWrapperProps } from 'components/ResponsiveIframeWrapper';

export interface ResponsivePlayerProps {
  reactPlayerProps: Omit<ReactPlayerProps, 'width' | 'height'>;
  responsiveIframeWrapperProps: ResponsiveIframeWrapperProps;
}

export const ResponsivePlayer: React.FC<ResponsivePlayerProps> = ({
  responsiveIframeWrapperProps,
  reactPlayerProps,
}) => (
  <ResponsiveIframeWrapper {...responsiveIframeWrapperProps}>
    <ReactPlayer {...reactPlayerProps} width="100%" height="100%" />
  </ResponsiveIframeWrapper>
);

export default ResponsivePlayer;
