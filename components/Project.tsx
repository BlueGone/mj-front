import * as React from 'react';
import { Layout, Typography, Row, Col } from 'antd';
import { Project as ProjectModel } from 'models/Project';
import { resolveMediaUrl } from 'models/strapiHelpers';

const Project: React.FC<ProjectModel> = (project) => {
  console.log({ desc: project.description });
  return <Layout.Content style={{ padding: '0 50px' }} >
    <Row type="flex" justify="center">
      <Col xs={22} md={18} lg={14} style={{ whiteSpace: "pre-wrap", textAlign: "center" }}>
        <Typography.Title className="dark" level={2}>{project.title}</Typography.Title >
      </Col>
    </Row>
    <Row type="flex" justify="center">
      <Col xs={22} md={18} lg={14} style={{ whiteSpace: "pre-wrap", textAlign: "center" }}>
        <Typography.Paragraph className="dark">{project.description}</Typography.Paragraph>
      </Col>
    </Row>
    <Row type="flex" justify="center">
      <Col xs={22} md={18} lg={14} style={{ whiteSpace: "pre-wrap", textAlign: "center" }}>
        <Typography.Paragraph className="dark">{project.credits}</Typography.Paragraph>
      </Col>
    </Row>
    {
      <Row type="flex" justify="center" gutter={[16, 16]}>
        { project.images.length >= 1
          ? <Col xs={20}><img width="100%" src={resolveMediaUrl(project.images[0])} alt="" /></Col>
          : null
        }
        { project.images.length >= 2
          ? project.images.length >= 3
            ? <>
                <Col xs={10}><img width="100%" src={resolveMediaUrl(project.images[1])} alt="" /></Col>
                <Col xs={10}><img width="100%" src={resolveMediaUrl(project.images[2])} alt="" /></Col>
              </>
            : <Col xs={20}><img width="100%" src={resolveMediaUrl(project.images[1])} alt="" /></Col>
          : null
        }
        { project.images.slice(3).map((image) => <Col xs={20}><img width="100%" src={resolveMediaUrl(image)} key={resolveMediaUrl(image)} alt="" /></Col>) }
      </Row>
    }
  </Layout.Content>
};

export default Project;
