import React, { useState } from 'react';
import { Layout, Row, Col } from 'antd';
import { HamburgerButton } from 'react-hamburger-button';

import { Logo } from 'components/Logo';
import FullscreenMenu from './FullscreenMenu';

export const Header: React.FC = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  return (
    <>
      <FullscreenMenu visible={isMenuOpen} />
      <Layout.Header>
        <Row type="flex" justify="center" align="middle">
          <Col xs={4} sm={3} lg={2}>
            <Logo />
          </Col>
          <Col xs={16} sm={18} lg={20} />
          <Col xs={4} sm={3} lg={2}>
            <div style={{
              width: '100%', display: 'inline-flex', justifyContent: 'center', alignItems: 'center',
            }}
            >
              <div style={{ cursor: 'pointer', zIndex: 2 }}>
                <HamburgerButton
                  open={isMenuOpen}
                  onClick={(): void => setIsMenuOpen(!isMenuOpen)}
                  color="#fff"
                />
              </div>
            </div>
          </Col>
        </Row>
      </Layout.Header>
    </>
  );
};

export default Header;
