import * as React from 'react';

export interface ResponsiveIframeWrapperProps {
  aspectRatio: number;
}

export const ResponsiveIframeWrapper: React.FC<ResponsiveIframeWrapperProps> = ({
  aspectRatio,
  children,
}) => (
  <>
    <style jsx>
      {`
        .responsive-iframe-wrapper {
          overflow: hidden;
          position: relative;
        }
        
        .responsive-iframe-wrapper .responsive-iframe-wrapper-content {
          position: absolute;
          left: 0;
          top: 0;
          width: 100%;
          height: 100%;
        }
      `}
    </style>
    <div className="responsive-iframe-wrapper" style={{ paddingTop: `${100 / aspectRatio}%` }}>
      <div className="responsive-iframe-wrapper-content">
        { children }
      </div>
    </div>
  </>
);

export default ResponsiveIframeWrapper;
