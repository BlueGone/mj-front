import * as React from 'react';
import {
  Layout,
  Row,
  Col,
  Typography,
} from 'antd';

import localesJson from 'locales.json';
import aboutJson from 'about.json';

export const Contact: React.FC = () => (
  <Layout.Content style={{ padding: '0 50px' }}>
    <Row type="flex" justify="start" align="middle">
      <Col xs={0} md={0} lg={2} />
      <Col xs={24} md={24} lg={20}>
        <Typography.Title>{localesJson.SEE_YOU_SOON}</Typography.Title>
      </Col>
      <Col xs={0} md={0} lg={2} />
    </Row>
    <Row type="flex" justify="center" align="middle">
      <Col xs={24} md={8} lg={8}>
        <img className="rounded" width="100%" height="100%" alt="profile_picture" src="/images/profile_picture.jpg" />
      </Col>
      <Col xs={0} md={2} lg={2} />
      <Col xs={24} md={14} lg={10}>
        <a href={`mailto:${aboutJson.EMAIL}`}>
          <Typography.Paragraph>{aboutJson.EMAIL}</Typography.Paragraph>
        </a>
        <a href={`tel:${aboutJson.PHONE_NUMBER}`}>
          <Typography.Paragraph>{aboutJson.PHONE_NUMBER}</Typography.Paragraph>
        </a>
      </Col>
    </Row>
  </Layout.Content>
);

export default Contact;
