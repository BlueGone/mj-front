import React from 'react';
import {
  Row, Col, Layout, Typography,
} from 'antd';

import {
  PROJECT_TYPE_MOTION_DESIGN,
  PROJECT_TYPE_INTERACTIVE,
  PROJECT_TYPE_VIDEO_LABORATORY,
} from '../locales.json';

export interface FullscreenMenuProps {
  visible: boolean;
}

const FullscreenMenu: React.FC<FullscreenMenuProps> = ({ visible }) => (
  <Layout className={`fullscreen-menu-root ${visible ? 'visible' : 'hidden'}`}>
    {
      [
        PROJECT_TYPE_MOTION_DESIGN,
        PROJECT_TYPE_INTERACTIVE,
        PROJECT_TYPE_VIDEO_LABORATORY,
      ].map((title) => (
        <Row type="flex" key={title} align="middle" justify="center">
          <Col xs={18} md={14} lg={10}>
            <Typography.Title level={2}>{title}</Typography.Title>
          </Col>
        </Row>
      ))
    }
  </Layout>
);

export default FullscreenMenu;
