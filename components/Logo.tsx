import * as React from 'react';
import Link from 'next/link';

export const Logo: React.FC = () => (
  <Link href="/">
    <a href="/">
      <img height="100%" width="100%" alt="logo" src="/images/logo.png" />
    </a>
  </Link>
);

export default Logo;
