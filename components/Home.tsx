import * as React from 'react';
import { Layout } from 'antd';
import { ResponsivePlayer } from 'components/ResponsivePlayer';
import { Homevideo } from 'models/Homevideo';

export interface HomeProps {
  homevideos: Homevideo[];
}

export const Home: React.FC<HomeProps> = ({ homevideos }) => (
  <Layout.Content style={{ padding: '0 50px' }}>
    {
        homevideos.map((homeVideo) => (
          <ResponsivePlayer
            reactPlayerProps={{ url: homeVideo.url }}
            responsiveIframeWrapperProps={{ aspectRatio: 16 / 9 }}
            key={homeVideo.url}
          />
        ))
      }
  </Layout.Content>
);

export default Home;
