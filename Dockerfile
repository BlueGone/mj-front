FROM node:13 AS base
WORKDIR /app

FROM base as all-dependencies
COPY [ "package.json", "package-lock.json", "./" ]
RUN [ "npm", "install" ]

FROM base as prod-dependencies
COPY [ "package.json", "package-lock.json", "./" ]
RUN [ "npm", "install", "--production" ]

FROM base as build
COPY --from=all-dependencies [ "/app/node_modules/", "node_modules/" ]
COPY [ "components/", "components/" ]
COPY [ "models/", "models/" ]
COPY [ "pages/", "pages/" ]
COPY [ "about.json", "config.json", "locales.json", "next.config.js", "package.json", "tsconfig.json", "./" ]
RUN [ "npm", "run", "build" ]

FROM base as run
COPY --from=prod-dependencies [ "/app/node_modules/", "node_modules/" ]
COPY --from=build [ "/app/.next", ".next/" ]
COPY [ "public/", "public/" ]
COPY [ "package.json", "./" ]
CMD [ "npm", "run", "start" ] 
